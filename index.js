
const timer = document.getElementById('stopwatch');

var min = 0;
var sec = 0;
var key = 0;
var ids = 0;
var stoptime = true;
var timerValue = '00:00';

start.onclick = function() {
    if (start.textContent == 'Pause') {
        start.textContent = 'Start';
    }else {
        start.textContent = 'Pause';
    }
    if (stoptime == true) {
        stoptime = false;
        timerCycle();
        return
    }
    if (stoptime == false) {
        stoptime = true;
    }
    key++;
    localStorage.setItem('key'+key, timerValue);
}
add.onclick = function() {
    ids++
    div = document.createElement("div")
    pdiv = document.createElement("div")
    p = document.createElement("p")
    p.textContent = timerValue
    b = document.createElement("button")
    b.textContent = 'X'
    b.setAttribute("onclick",'removeItem('+ids+')')
    div.setAttribute("id",'item'+ids)
    pdiv.appendChild(p)
    div.appendChild(pdiv)
    div.appendChild(b)
    result.appendChild(div)
}
let removeItem = (id)=>{
    divid = document.getElementById('item'+id)
    divid.remove()
}
function nima(){
    for (let i=1;i<=ids;i++){

        del = 'del' + i
        delid = document.getElementById(del)
        
    }
}
nima()
function timerCycle() {
    if (stoptime == false) {
        sec = parseInt(sec);
        min = parseInt(min);

        sec = sec + 1;

        if (sec == 60) {
            min = min + 1;
            sec = 0;
        }

        if (sec < 10 || sec == 0) {
            sec = '0' + sec;
        }
        if (min < 10 || min == 0) {
            min = '0' + min;
        }
        timerValue = min + ':' + sec;
        timer.innerHTML = timerValue;

        setTimeout("timerCycle()", 1000);
    }
}

reset.onclick = function() {
    timer.innerHTML = '00:00';
    min = 0;
    sec = 0;
    key = 0;
    start.textContent = 'Start';
    if (stoptime == false) {
        stoptime = true;
    }
    localStorage.clear();
    
}